package ru.tsc.borisyuk.tm.exception.user;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error. User Id is empty.");
    }

}
