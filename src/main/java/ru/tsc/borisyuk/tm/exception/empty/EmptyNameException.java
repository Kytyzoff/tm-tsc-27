package ru.tsc.borisyuk.tm.exception.empty;

import ru.tsc.borisyuk.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error. Name  cannot take in an empty String or null value.");
    }

}
