package ru.tsc.borisyuk.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    @Nullable void setStartDate(@NotNull Date startDate);

}
