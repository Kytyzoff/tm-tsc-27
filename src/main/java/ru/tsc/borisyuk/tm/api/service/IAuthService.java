package ru.tsc.borisyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.enumerated.Role;
import ru.tsc.borisyuk.tm.model.User;

public interface IAuthService {

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void checkRoles(@Nullable Role... roles);

}
