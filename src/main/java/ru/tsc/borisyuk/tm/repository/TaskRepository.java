package ru.tsc.borisyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(t -> t.getId().equals(name))
                .filter(t -> t.getUserId().equals(userId))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        entities.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @NotNull
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @NotNull
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @NotNull
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @NotNull
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name, @NotNull final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Nullable
    @Override
    public Task unbindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        final Task task = findByProjectAndTaskId(userId, projectId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Nullable
    @Override
    public Task findByProjectAndTaskId(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (taskId.equals(task.getId()) && projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @NotNull
    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            entities.remove(task);
        }
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }

}
